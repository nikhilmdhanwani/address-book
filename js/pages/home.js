$(function() {
    $('.modal').modal();
    $('.delete-btn').click(function(){
        const idToBeDeleted = $(this)[0].dataset.id;
        $("#delete_textfield").val(idToBeDeleted);
    });

    function getQueryStrings() {
        let queryStrings = window.location.search.substring(1).split("&") || [];
        let vars = {}, hash;
        queryStrings.forEach(query => {
            hash = query.split("=");
            vars[hash[0]] = hash[1];
        });
        return vars;
    }

    const queryParams = getQueryStrings();
    const operation = queryParams.op;
    const status = queryParams.status;
    if(operation && operation == "insert"){
        if(status == "success") {
            M.toast({html: 'Contact Added Successfully!', classes: 'green darken-1'});
        }
    }
    else if(operation && operation == "update"){
        if(status == "success") {
            M.toast({html: 'Contact Updated Successfully!', classes: 'green darken-1'});
        }
    }
    else if(operation && operation == "delete"){
        if(status == "success") {
            M.toast({html: 'Contact Deleted Successfully!', classes: 'green darken-1'});
        }
    }
});