<?php
require_once("./includes/functions.inc.php");

if(!isset($_POST['contact_id_to_be_deleted'])) {
    die("How the hell you reached here?");
}
    $id = $_POST['contact_id_to_be_deleted'];
    //Delete the image!
    $image_name = db_select("SELECT image_name FROM contacts WHERE id = $id")[0]['image_name'];
    $file_name = get_image_file_name($image_name, $id);

    unlink("images/users/$file_name");
    $query = "DELETE FROM contacts WHERE id = $id";
    db_query($query);

    redirect("index.php?op=delete&status=success");