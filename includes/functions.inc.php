<?php
    function db_connect() {
        static $connection;

        if(!$connection) {
            $config = parse_ini_file("./config.ini"); //to parse a configuration file
            $connection = mysqli_connect($config["host"], $config["username"], $config["password"],  $config["database"], $config["port"]); //Opens a new connection to MySQL server
        }
        return $connection;
    }


    function dd($data) {
        //die prints a message and terminates the script
        die(var_dump($data));
    }

    function db_query($query) {
        $connection = db_connect();
        $result = mysqli_query($connection, $query); //Performs a query on the database 
        return $result;
    }

    function db_select($select_query) {
        $result = db_query($select_query);
        if(!$result){
            return false;
        }

        $rows = array();
        while($row = mysqli_fetch_array($result)){ //Fetch a result row as a numeric array and as an associative array
            $rows[] = $row;
        }
        return $rows;
    }
        
        function sanitize($value){
            $connection = db_connect();
            //trim to remove extra spaces to avoid redundancy 
            return trim(mysqli_real_escape_string($connection, $value));
        }

        function prepare_insert_query($table_name, $data){
            $values = array_values($data);
            $keys = array_keys($data);
            for($i=0; $i<count($values); $i++){
                $values[$i] = "'" . $values[$i] . "'";
            }
            //Returns a string from the elements of the array
            $valuesString = implode(", ", $values);
            $keysString = implode(", ", $keys);
            // dd($keysString);      
            
            return "INSERT INTO $table_name ($keysString) VALUES($valuesString);";
        }

        function old($collection, $key, $default_value=""){
            //old datatset
            return trim(isset($collection[$key]) ? $collection[$key] : $default_value);
        }

        function get_last_insert_id(){
            //for assigning unique values
            $connection = db_connect();
            return mysqli_insert_id($connection);
        }

        function redirect($url){
            //redirection after submitting
            //sends raw http header to the client
            header("Location: $url");
        }

        function get_image_file_name($image_name, $id) {
            //explode breaks a string to array
            //$len = count(explode(".", $image_name));
            //return $len == 1 ? "$id.$image_name" : $image_name;
            return strpos($image_name, ".") ? $image_name : "$id.$image_name";
        }

        function prepare_update_query($table_name, $data, $condition){
            //UPDATE TABLE set column1 = 'value1' , column = value2,... WHERE condition;

            $values = "";

            foreach($data as $key=>$value) {
                $values = $values . "$key = '$value',";
            }
            $values = rtrim($values, ", ");
            $query = "UPDATE $table_name SET $values WHERE $condition";
            return $query;
        }
?>