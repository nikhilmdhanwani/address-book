<?php 
    require_once("./includes/functions.inc.php"); //For connection
    $error = false;
    if(isset($_POST['action'])){  
        //POST takes data in array format as key value pair, where key is the name given and value is the value given in attributes 
        //But similiary if you send it from URL or hit enter from URL its an GET request not POST
        //you pressed submit button
        //_POST is also a super global variable which is used to collect form data after submitting an HTML form with method=post, mainly used to pass variables
        $first_name = sanitize($_POST['first_name']);
        $last_name = sanitize($_POST['last_name']);
        $email = sanitize($_POST['email']);
        $telephone = sanitize($_POST['telephone']);
        $address = sanitize($_POST['address']);
        //birthdate format of PHP
        $birthdate = !empty($_POST['birthdate']) ? date("Y-m-d", strtotime(sanitize($_POST['birthdate']))) : "";
        //_FILES is another super global variable which is an associative array of items uploaded to the current script via POST method.
        if(!$first_name || !$last_name || !$email || !$telephone || !$address || !$birthdate || !isset($_FILES['pic']['name'])) {
            $error = true;
        } else {
            $og_file_name = $_FILES['pic']['name'];
            $ext = end(explode(".", $og_file_name));
            $tmp_path = $_FILES['pic']['tmp_name'];

            $data['first_name'] = $first_name;
            $data['last_name'] = $last_name;
            $data['email'] = $email;
            $data['telephone'] = $telephone;
            $data['address'] = $address;
            $data['birthdate'] = $birthdate;
            $data['image_name'] = $ext;
            $query = prepare_insert_query("contacts", $data);

            db_query($query);

            $contact_id = get_last_insert_id();

            move_uploaded_file($tmp_path,"images/users/$contact_id.$ext");

            redirect("index.php?op=insert&status=success");
        }
    }
?>
<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Import Csutom CSS-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Add Contact</title>
</head>

<body>
    <?php 
        include_once("./includes/navbar.inc.php");
    ?>
    <div class="container">
        <div class="row mt50">
            <h2>Add New Contact</h2>
        </div>
        <?php
            if($error):
        ?>
        <div class="row">
            <div class="materialert error">
                <div class="material-icons">error_outline</div>
                Oh! What a beautiful alert :)
                <button type="button" class="close-alert">×</button>
            </div>
        </div>
        <?php 
        endif;
        ?>
        <div class="row">
            <form class="col s12 formValidate" action="<?= $_SERVER['PHP_SELF']; ?>" id="add-contact-form" method="POST" enctype="multipart/form-data">
                <div class="row mb10">
                    <div class="input-field col s6">
                        <input id="first_name" name="first_name" type="text" class="validate" data-error=".first_name_error" value="<?= old($_POST, 'first_name');?>">
                        <label for="first_name">First Name</label>
                        <div class="first_name_error error"><?= isset($first_name) && empty($first_name) ? "Please Fill" : "" ?></div>
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" name="last_name" type="text" class="validate" data-error=".last_name_error" value="<?= old($_POST, 'last_name');?>">
                        <label for="last_name">Last Name</label>
                        <div class="last_name_error error"><?= isset($last_name) && empty($last_name) ? "Please Fill" : "" ?></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="input-field col s6">
                        <input id="email" name="email" type="email" class="validate" data-error=".email_error" value="<?= old($_POST, 'email');?>">
                        <label for="email">Email</label>
                        <div class="email_error error"><?= isset($email) && empty($email) ? "Please Fill" : "" ?></div>
                    </div>
                    <div class="input-field col s6">
                        <input id="birthdate" name="birthdate" type="text" class="datepicker" data-error=".birthday_error" value="<?= old($_POST, 'birthdate');?>">
                        <label for="birthdate">Birthdate</label>
                        <div class="birthday_error error"><?= isset($birthdate) && empty($birthdate) ? "Please Fill" : "" ?></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="input-field col s12">
                        <input id="telephone" name="telephone" type="tel" class="validate" data-error=".telephone_error" value="<?= old($_POST, 'telephone');?>">
                        <label for="telephone">Telephone</label>
                        <div class="telephone_error error"><?= isset($telephone) && empty($telephone) ? "Please Fill" : "" ?></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="input-field col s12">
                        <textarea id="address" name="address" class="materialize-textarea" data-error=".address_error"><?= old($_POST, 'address');?></textarea>
                        <label for="address">Addess</label>
                        <div class="address_error error"><?= isset($address) && empty($address) ? "Please Fill" : "" ?></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="file-field input-field col s12">
                        <div class="btn">
                            <span>Image</span>
                            <input type="file" name="pic" id="pic" data-error=".pic_error">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="Upload Your Image">
                        </div>
                        <div class="pic_error error"></div>
                    </div>
                </div>
                <button class="btn waves-effect waves-light right" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                    </button>
            </form>
        </div>
    </div>
    <footer class="page-footer p0">
        <div class="footer-copyright ">
            <div class="container">
                <p class="center-align">© 2020 Study Link Classes</p>
            </div>
        </div>
    </footer>
    <!--JQuery Library-->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--JQuery Validation Plugin-->
    <script src="vendors/jquery-validation/validation.min.js" type="text/javascript"></script>
    <script src="vendors/jquery-validation/additional-methods.min.js" type="text/javascript"></script>
    <!--Include Page Level Scripts-->
    <script src="js/pages/add-contact.js"></script>
    <!--Custom JS-->
    <script src="js/custom.js" type="text/javascript"></script>
</body>

</html>